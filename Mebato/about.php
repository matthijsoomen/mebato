<!doctype html>
<html>

<head>
	<html lang="en-US">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/bootstrap-extend.min.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="assets/js/scroll-up.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Mebato b.v.</title>
</head>

<body class="homepage">
	<div class="page-wrapper">
		<header id="page-header">
			<?php include("assets/templates/header.php"); ?>
		</header>
	</div>
	<div class="main-wrapper" id="main-wrapper">
		<a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>
		<section class="section-company pt-100 pb-50">
			<div class="container text-center">
				<span class="heading">Wie is Mebato en waar staat dat voor?</span>

			</div>
		</section>

		<section class="section-content">
			<div class="content-wrapper p-50">
				<center><img src="assets/img/img0024.png" class="py-30">
				</center>
				<div class="row py-20">
					<div class="col-md-6" id="wrapper-text">
						<p>
							<span class="drop-cap" id="firstcap">M</span>echanisch; management project en proces; markt; milieubewust.</p>
						<span class="drop-cap" id="firstcap">E</span>lectrisch; energiek; één is één, samen is sterk.</p>
						<span class="drop-cap" id="firstcap">B</span>edrijf; bekendheid markt; bekendheid werk; belangen.</p>
						<span class="drop-cap" id="firstcap">A</span>dvies; actueel; altijd vernieuwend en innovatief adviserend consultant; constructief; creatief en dynamisch.</p>
						<span class="drop-cap" id="firstcap">T</span>echniek; tekenen; tesamen; ter zake kundig; TCO expertise.</p>
						<span class="drop-cap" id="firstcap">O</span>plossingen; onderzoekend.</p>
					</div>
					<div class="col-md-6" id="wrapper-text-left">
						<p>
							“Mebato” is een onafhankelijk en dienstverlenend expertise bureau dat in 2015 is opgericht als een “industrial solution partner for material handling and process logistics”.</p><br>
						<p>Over een breed front kan “Mebato” uw organisatie of project ondersteunen, zowel op technisch-uitvoerend als op technisch-organisatorisch gebied.</p><br>
						<p>Kiezen voor “Mebato” is kiezen voor bijna 35 jaar ervaring op het gebied van multidiscilplinaire werktuigbouwkundige expertise van installatie onderdelen tot aan complete turn-key installaties.</p><br>
						<p>“Mebato” richt zich met name op het midden- en kleinbedrijf en hanteert een persoonlijke en klantvriendelijke aanpak, die gericht is op uw bedrijf!</p>
					</div>
				</div>
			</div>
		</section>


	</div>

	<div>
		<?php include("assets/templates/footer.php"); ?>
	</div>



</body>

</html>