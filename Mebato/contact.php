<!doctype html>
<html>

<head>
	<html lang="en-US">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/scss/google-maps.scss">
	<link rel="stylesheet" href="assets/css/bootstrap-extend.min.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/assets/js/scroll-up.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://maps.google.com/maps/api/js?key=AIzaSyC4yeNR28SxOgqTQeYb_WiSKz5H2Wx8O9M" type="text/javascript"></script>
	<title>Mebato b.v.</title>
</head>

<body class="homepage">
	<div class="page-wrapper">
		<div id="page-header">
			<?php include("assets/templates/header.php"); ?>
		</div>
	</div>
	<div class="main-wrapper" id="main-wrapper">
			<a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>
		<section class="section-company pt-100 pb-50">
			<div class="container text-center">
				<span class="heading">Hoe met Mebato in contact te komen.</span>

			</div>
		</section>

		<section class="section-content">
			<div class="content-wrapper p-50">

					<div class="col-md-12 mt-70" id="address-info">
						<center><h3>Mebato b.v</h3>
						<p> | <span class="glyphicon glyphicon-map-marker" style="width=20px;height=20px"></span> Bingerweg 18D , 2031AZ Haarlem | <span class="glyphicon glyphicon-earphone" style="width:20px;height:20px"></span> Tel. +31 (0)85 877 07 32 | <span class="glyphicon glyphicon-envelope" style="width:20px;height:20px"></span> info@mebato.nl </p>
						<hr>
						<h3>KvK & Btw</h3>
						<p> | <span class="glyphicon glyphicon-briefcase" style="height:20px;width:20px"></span> <strong>KvK</strong> 63421119 | <span class="glyphicon glyphicon-briefcase" style="height:20px;width:20px"></span> <strong>BTW</strong> nr: NL855228398B01 </p> </center>
					</div>
			</div>
		</section>
<section id="cd-google-map">
						<div class="content-wrapper-maps">
							<div id="google-container"></div>
							<div id="cd-zoom-in"></div>
							<div id="cd-zoom-out"></div>
						</div>
					</section>

	</div>

	</div>

	<div>
		<?php include("assets/templates/footer.php"); ?>
	</div>
	<script src="assets/js/google-maps.js"></script>

</body>

</html>