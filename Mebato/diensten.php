<!doctype html>
<html>

<head>
	<html lang="en-US">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/bootstrap-extend.min.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="assets/js/scroll-up.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Mebato b.v.</title>
</head>


<body class="homepage">
	<div class="page-wrapper">
		<header id="page-header">
			<?php include("assets/templates/header.php"); ?>
		</header>
	</div>
	<div class="main-wrapper" id="main-wrapper">
			<a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>
		<section class="section-company pt-100 pb-50">
			<div class="container text-center">
				<span class="heading"><strong>Diensten.</strong> Deze kunnen wij voor u uitvoeren.</span>

			</div>
		</section>

		<section class="section-content">
			<div class="content-wrapper p-50">
				<div class="row">
				<div class="col-md-8">
				<div class="row">
					<div class="col-md-6 wrapper-txt">
						<h4 class="text-title text-center">Projectmanagement</h4><br>
		<p> De organisatie van een project bepaald voor een groot deel het rendement van een project.</p> <br> <p>“Mebato” beschikt over ervaring in de realisatie van multidisciplinaire projecten en kan de organisatie en begeleiding van een project op zich nemen (brown field en green field projecten).</p> <br><p> Projecten zullen bij voorkeur uitgevoerd worden volgens Prince2.</p>
	</div>
	
		
	<div class="col-md-6 wrapper-txt">
		<h4 class="text-title text-center">Projectengineering</h4><br>
		<p>Voor het welslagen en de beheersbaarheid van een project is een juiste project definitie van groot belang.</p><br>
<p>Factoren als de projectinhoud, de projectcalculatie (projectbudget) en de projectplanning zijn bepalend voor de sturing en controle op het rendement en de doorlooptijd van het project.</p><br>
<p>“Mebato” beschikt over de kennis en ervaring om de juiste stappen te zetten.</p>
	</div>
					</div>
					
					<div class="row">
					<img src="assets/img/idea-def.jpg" class="idea-def">
					</div>
					</div>
					
	<div class="col-md-4 wrapper-txt">
		<h4 class="text-title text-center">Projectondersteuning</h4><br>
		<p>“Mebato” kan ook ondersteuning bieden op onderdelen van een project.</p><br>
		<p>Meer concreet kan daarbij worden gedacht aan bijvoorbeeld:</p><br>
		<ul>
			<li>advies technisch en organisatorisch</li>

			<li>scope definiëren van project</li>

<li>opstellen van projectspecificaties</li>

<li>opstellen systeemspecificaties</li>

<li>planning van de projectwerkzaamheden</li>

<li>engineering van het project</li>

<li>begeleiding bij het productietraject</li>

<li>organiseren of begeleiding bij testen</li>

<li>begeleiding bij CE-markering (risicoanalyse)</li>

<li>schrijven van handleidingen</li>

<li>schrijven van procesbeschrijvingen</li>

<li>begeleiding bij implementatie</li>

<li>begeleiding inbedrijfstelling</li>
		</ul>
	</div>
</div>
			</div>
		</section>
	</div>

<div>
 	<?php include("assets/templates/footer.php"); ?>
 </div>
		</body>