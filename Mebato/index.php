<!doctype html>
<html>

<head>
	<html lang="en-US">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/bootstrap-extend.min.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="assets/js/scroll-up.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<title>Mebato b.v.</title>
</head>




<body>

	<body class="homepage">
		<div class="page-wrapper">
			<header id="page-header">
				<?php include( "/assets/templates/header.php"); ?>
			</header>
		</div>
		<div class="main-wrapper" id="main-wrapper">
			<a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>
			<section class="section-company pt-100 pb-50">
				<div class="container text-center">

					<span class="companyName">Mebato</span>
					<br>
					<span class="sloganName">Your Industrial Solution Partner</span>
				</div>
			</section>

			<section class="section-content">
				<div class="content-wrapper p-50">
					<center><img src="assets/img/img0024.png" class="py-30">
					</center>
					<div class="text-center" id="wrapper-text">"Mebato" is gevestigd in een modern bedrijfsverzamel gebouw dat ligt aan de buitenrand van het industrieterrein De Waarderpolder in Haarlem.
						<br><br> Vanuit dit kantoor worden alle activiteiten uitgevoerd, begeleid en gecontroleerd om tot gevraagde eindresultaat te komen voor u als gewaardeerde opdrachtgever.</div>
				</div>
			</section>

		</div>

		<div>
			<?php include("/assets/templates/footer.php"); ?>
		</div>

	</body>


	</html>