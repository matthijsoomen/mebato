<footer>

	<section>
		<div class="container-fluid page-footer">
			<div class="col-md-9 col-sm-6 col-xs-6 my-25 pt-10">
				<div class="copyright">
					<p>Copyright © 2016 by 'Mebato b.v.'</p>
					<p><em>All rights reserved</em></p>
				</div>

			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="row text-center">
					<h4 class="footer-heading">Mebato b.v.</h4>
				</div>
				<div class="row text-center mt--10">
 					<div class="col-md-6 text-right page-footer">
						<span class="footer-info">Tel +31 (0)85 877 07 32<br>
						info@mebato.nl</h4>
					</div>
					<div class="col-md-6 text-left page-footer">
						<span class="footer-info">Bingerweg 18D<br>
						2031 AZ Haarlem</h4>
					</div>
				</div>
			
			</div>
			</div>

	</section>


	
</footer>