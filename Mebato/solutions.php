<!doctype html>
<html>

<head>
	<html lang="en-US">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/css/bootstrap-extend.min.css" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="/assets/js/scroll-up.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://masonry.desandro.com/masonry.pkgd.js"></script>
	<title>Mebato b.v.</title>
</head>

<body class="homepage">
	<div class="page-wrapper">
		<header id="page-header">
			<?php include("assets/templates/header.php"); ?>
		</header>
	</div>
	<div class="main-wrapper" id="main-wrapper">
		<a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>
		<section class="section-company pt-100 pb-50">
			<div class="container text-center">
				<span class="heading"><strong>Solutions.</strong> Welke diensten kunnen wij u bieden. </span>

			</div>
		</section>

		<section class="section-content">
			<div class="content-wrapper p-50">
				<div class="row">
					<h3>
						<center>Mebato laat de raderen van uw organisatie draaien</center>
					</h3>
					<br>
					<img src="assets/img/wheels.jpg" alt="" id="img-header">
					<br>

				</div>
				<div class="row">

					<div class="col-md-3 wrapper-txt">
						<h4 class="text-title text-center">Engineering</h4> Engineering omvat de diensten van project management, concept-, basic- en detail engineering
					</div>

					<div class="col-md-3 wrapper-txt">
						<h4 class="text-title text-center">Verspaning</h4> Verspaning is vooral gericht op enkelstuks fabrikage, grotere series worden commercieel in de markt uitbesteed
					</div>

					<div class="col-md-3 wrapper-txt">
						<h4 class="text-title text-center">Constructie</h4> Constructie werkplaats is goed geoutilleerd en ook het assembleren voor een FAT test behoren tot de mogelijkheden
					</div>

					<div class="col-md-3 wrapper-txt">
						<h4 class="text-title text-center">Elektro/Automatisering</h4> Automatiseringsvraagstuk van schakelkasten en besturing kunnen in eigen beheer worden uitgevoerd. Resulteert daarmee in korte lijnen.
					</div>

				</div>
				<div class="container">
					<hr class="gallery-divider">
				</div>
				<div class="row">
					<div class="grid">
						<div class="grid-sizer"></div>

						<div class="grid-item">
							<img src="https://images.pexels.com/photos/345415/pexels-photo-345415.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/350351/pexels-photo-350351.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/351435/pexels-photo-351435.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/349493/pexels-photo-349493.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/146572/pexels-photo-146572.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/341378/pexels-photo-341378.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/261064/pexels-photo-261064.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
						<div class="grid-item">
							<img src="https://images.pexels.com/photos/162693/plums-fruit-jam-violet-162693.jpeg?w=940&h=650&auto=compress&cs=tinysrgb" alt="">
						</div>
					</div>
				</div>


			</div>
		</section>


	</div>

	<div>
		<?php include("assets/templates/footer.php"); ?>
	</div>

	<style>
	* {
			box-sizing: border-box;
		}
		
		.grid {
			max-width: 1115px;
			margin: 0 auto;
		}
		
		.grid-sizer,
		.grid-item {
			width: 275px;
		}
		
		.grid-item img {
			max-width: 100%;
			border-radius: 5px;
			padding-top: 5px;
			padding-left: 0;
		}
		
		.gallery-divider {
			border: 1px solid;
		}
		
		#img-header {
			height: 10%;
			width: 20%;
			display: block;
			margin-left: auto;
			margin-right: auto;
		}
	</style>

	<script>
		$( document ).ready( function () {
			var $grid = $( '.grid' ).masonry( {
				itemSelector: '.grid-item',
				columnWidth: '.grid-sizer',
				gutter: 5
			} );
		} );
	</script>

</body>

</html>