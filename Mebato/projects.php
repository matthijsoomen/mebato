<!doctype html>
<html>

<head>
	<html lang="en-US">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/style.css">
	<link rel="stylesheet" href="assets/css/carousel.css">
	<link rel="stylesheet" href="assets/css/bootstrap-extend.min.css" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="assets/js/scroll-up.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<title>Mebato b.v.</title>

</head>


<body class="homepage">

	<div class="page-wrapper">
		<header id="page-header">
			<?php include("assets/templates/header.php"); ?>
		</header>
	</div>

	<div class="main-wrapper" id="main-wrapper">
		<a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>
		<section class="section-company pt-100 pb-50">
			<div class="container text-center">
				<span class="heading"><strong>Projecten.</strong> Slechts een greep uit wat we gedaan hebben.</span>

			</div>
		</section>

		<section class="section-content">
			<div class="content-wrapper p-50">

				<div class="row">
					<div class="col-md-4 py-15">
						<img src="assets/img/img0024.png" style="float:right;">
					</div>
					<div class="col-md-8 py-15" id="wrapper-text">
						<h3><strong>PROJECTS</strong></h3> Mebato verricht zowel mono disciplinaire projecten als multidisciplinaire projecten. Onderstaand staan slechts wat voorbeelden van projecten die wij voor en samen onze klanten uitgevoerd hebben.
					</div>
				</div>

			
			</div>
		</section>
	<div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="5000">
					<ol class="carousel-indicators" id="flat-indicators">
						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
						<li data-target="#myCarousel" data-slide-to="1"></li>
						<li data-target="#myCarousel" data-slide-to="2"></li>
						<li data-target="#myCarousel" data-slide-to="3"></li>
						<li data-target="#myCarousel" data-slide-to="4"></li>
						<li data-target="#myCarousel" data-slide-to="5"></li>
						<li data-target="#myCarousel" data-slide-to="6"></li>
						<li data-target="#myCarousel" data-slide-to="7"></li>
						<li data-target="#myCarousel" data-slide-to="8"></li>
						<li data-target="#myCarousel" data-slide-to="9"></li>
						<li data-target="#myCarousel" data-slide-to="10"></li>
					</ol>
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<img src="https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/4zx2oCU_lijl3z600/huge-industrial-factory-inside-robotic-machines-works_skldfcwc__F0000.png">
							<div class="carousel-caption"> Klantspecifiek ontwerpen van een menglijn. Concept, basic en detail engineering hiervoor gedaan.
							</div>
						</div>
						<div class="item">
							<img src="http://aurak.ac.ae/en/wp-content/uploads/2016/05/Industrial-1-1.jpg">
							<div class="carousel-caption"> Adviseren vervangen of optimaliseren van bedrijfskritische assets. Een transportband installatie analyseren op zijn kritieke aspecten en hoe deze te verbeteren.
							</div>
						</div>
						<div class="item">
							<img src="https://www.architectsjournal.co.uk/pictures/2000x2000fit/2/7/8/3047278_pg_14_151021_xxl_opera_3.jpg">
							<div class="carousel-caption"> Ontwerpen van machine opstelling om te komen tot veilige opstelling en optimale logistiek van in- en uitvoerstromen. Installatie voor de zware industrie. </div>
						</div>

						<div class="item">
							<img src="https://i.ytimg.com/vi/Bp4tGTNNi1I/maxresdefault.jpg">
							<div class="carousel-caption"> Ontwerpen van een nieuwe speciaal machine op basis van verzamelde informatie en specificatie.
							</div>
						</div>

						<div class="item">
							<img src="http://www.elasticspace.com/wp-content/uploads/2014/05/InternetMachine10-web.jpg">
							<div class="carousel-caption"> Haalbaarheidsonderzoek om proces installatie te optimaliseren, reduceren onderhouskosten en verbeteren veiligheid.
							</div>
						</div>

						<div class="item">
							<img src="http://mesc.solutions/upload/resyling.jpg">
							<div class="carousel-caption"> Onze grondstof 'afval' verwerken tot een nuttig product, te denken aan alternatieve energie of nuttige halffabrikaten.
							</div>
						</div>

						<div class="item">
							<img src="https://www.bezner.com/wp-content/uploads/2017/07/Recycling-plant-for-industrial-and-demolition-waste.jpg">
							<div class="carousel-caption"> Transportbanden in diverse uitvoeringen voor diverse applicaties ... wij willen u graag behulpzaam zijn bij de keuze, te denken aan; Vlakke banden | Staalkoordbanden | Boordbanden | PVC-PU banden enz.

							</div>
						</div>

						<div class="item">
							<img src="http://mesc.solutions/upload/resyling.jpg">
							<div class="carousel-caption"> Ook bestaande logistieke processen verbeteren of veiliger te maken. Hiermee levensduur en flexibiliteit verhogen, denk hierbij ook aan het systematisch en modulair denken bij conceptueel ontwerp.
							</div>
						</div>


						<div class="item">
							<img src="http://www.motiontronic.co.za/wp-content/uploads/2017/05/Industrial-Automation-South-Africa.jpg">
							<div class="carousel-caption"> Rollenbanen in alle denkbare varianten. Aangedreven, zwaartekracht gedreven, draaiplateua's enz enz. Om voor u te dimensioneren maar ook prijs gunstig leveren behoort tot de mogelijkheden.
							</div>
						</div>

						<div class="item">
							<img src="http://mesc.solutions/upload/resyling.jpg">
							<div class="carousel-caption"> Advies, ontwerp, realisatie en leveren van industriele drogers van stortgoederen. Van laboratorium tot realisatie. Ook optimalisatie van droogprocessen om tot energie reductie te komen. Kortom voor al uw droog vragen kunnen wij u helpen.
							</div>
						</div>




					</div><a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>
				</div>
	</div>

	<div>
		<?php include("assets/templates/footer.php"); ?>
	</div>


</body>

</html>