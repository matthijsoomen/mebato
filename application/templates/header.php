<header id="page-header">
	<div class="topnav" id="myTopnav">
		<div class="navbar-left"><a href="javascript:void(0);" ng-click="changePagina('index')" class="navbar-brand" id="logoName">Mebato <span class="hidden-sm-down" id="slogan"><em>Your Industrial Solutions Partner</em></span></a>
		</div>
		<div class="navbar-right collapse-menu" id="navbarRight"> <a href="javascript:void(0);" ng-click="changePagina('index')" class=" active hidden-sm-down">Home</a>
			<a href="javascript:void(0);" ng-click="changePagina('about')">Wie zijn wij</a>
			<a href="javascript:void(0);" ng-click="changePagina('diensten')">Diensten</a>
			<a href="javascript:void(0);" ng-click="changePagina('solutions')">Solutions</a>
			<a href="javascript:void(0);" ng-click="changePagina('projects')">Projects</a>
			<a href="javascript:void(0);" ng-click="changePagina('contact')">Contact</a>
			<div class="dropdown pull-left p-20 translationDown hidden-md-down">
				<button class="dropdown-toggle" type="button" id="menu1"  data-toggle="dropdown"><img src="public/icons/united-kingdom.png" height="30px" width="30px"> Translate
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
					<li role="presentation" class="menu2fl"><a role="menuitem" tabindex="-1" href="#"><img src="public/icons/germany.png" height="30px" width="30px"> GERMAN</a></li>
					<li role="presentation" class="menu2fl"><a role="menuitem" tabindex="-1" href="#"><img src="public/icons/france.png" height="30px" width="30px"> FRENCH</a></li>
				</ul>
			</div>
			<a href="javascript:void(0);" class="icon" onclick="myFunction()"><span class="glyphicon glyphicon-menu-hamburger" id="hamburger-menu"></span></a>
		</div>

	</div>
</header>