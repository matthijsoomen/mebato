<?php
session_start();

function generateRandomString($nbLetters){
    $randString="";
	$randomString="";
    $charUniverse="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for($i=0; $i<$nbLetters; $i++){
		$randInt=rand(0,61);
        $randChar=$charUniverse[$randInt];
        $randString.=$randomString.$randChar;
    }
    return $randString;
}

function cryptPassword($password, $salt, $rounds){
	// 16 character salt starting with $6$. The default number of rounds is 5000.
	if (CRYPT_SHA512 == 1){
		$codePassword = crypt($password,'$6$rounds='.$rounds.'$'.$salt.'$');
	}else{
		$codePassword = false;
	}
	return $codePassword;
}

$response = new stdClass();

$rounds =5000;
$saltSite = 'MG-OoMmoX@YzRo4M&a^D8LlCqQQuy&lK';

// Including database connections
require_once 'database_connections.php';

// Escaping special characters from updated data
$data = json_decode(file_get_contents("php://input"));
$action = mysqli_real_escape_string($con, $data->action);

if($action == 'login'){
	$ingevoerdeGebr = mysqli_real_escape_string($con, $data->gebruikersnaam);
	$pass = mysqli_real_escape_string($con, $data->wachtwoord);	

	// mysqli query to fetch all data from database
	$query = "SELECT * from `gebruikers`  WHERE `gebruikersnaam` = '$ingevoerdeGebr' AND `blok` != '1'";
	$result = mysqli_query($con, $query);
	$arr = array();
	if(mysqli_num_rows($result) != 0) {
		while($row = mysqli_fetch_assoc($result)) {
			$arr[] = $row;
		}
	}

	if(count($arr) != 0){
		$gebruikersnaam = $arr[0]['gebruikersnaam'];
		$wachtwoord = $arr[0]['wachtwoord'];
		$salt = $arr[0]['hash'];

		$ingevoerdePass = substr(cryptPassword($pass,$salt, $rounds), 32);
		$ingevoerdePass2 = substr(cryptPassword($ingevoerdePass,$saltSite, $rounds), 32);

		if($ingevoerdePass2 == $wachtwoord && $ingevoerdeGebr == $gebruikersnaam){
		
	 		$response->code = 100;
	 		$response->gebruikersnaam = $arr[0]['gebruikersnaam'];
	 		$response->email = $arr[0]['email'];
	 		$response->telefoon = $arr[0]['telefoon'];
	 		$gebruikerNaamS = $arr[0]['gebruikersnaam'];
	 		$gebruikerIdS = $arr[0]['id'];
	 		$gebruikerEmailS = $arr[0]['email'];

	 		include 'set_session.php';

		}else{
	 		$response->code = 301;
		}
	}else{
	 	$response->code = 401;
	}

	echo $json_info = json_encode($response);
}elseif($action == 'aanmelden'){
/*
	$ingevoerdeGebr = mysqli_real_escape_string($con, $data->gebruikersnaam);
	$pass = mysqli_real_escape_string($con, $data->wachtwoord);

	$queryBoek2 = "
   		INSERT INTO	`gebruikers` (`id`, `gebruikersnaam`,	`wachtwoord`, `hash`, `telefoon`, `email`, ``, ``, ``)
		 VALUES ('', '$gebruikersnaam', '$wachtwoord', '$hash', '$telefoon', '$email', '$', '$', '')
	";

	$result5 = mysqli_query($con, $queryBoek2);
	$boekId = $con->insert_id;

$pass = 'tirzaisdebaas';
$salt = generateRandomString(20);
$rounds =5000;

$saltSite = 'MG-OoMmoX@YzRo4M&a^D8LlCqQQuy&lK';

$password1 = substr(cryptPassword($pass,$salt, $rounds), 32);
$password2 = substr(cryptPassword($password1,$saltSite, $rounds), 32);

echo $salt;
echo '<br /><br />';
echo $password1;
echo '<br /><br />';
echo $password2;
echo '<br /><br />';
*/
}
?>