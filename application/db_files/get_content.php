<?php
// Including database connections
require_once 'database_connections.php';

// Fetching the updated data & storin in new variables
$data = json_decode(file_get_contents("php://input"));
// Escaping special characters from updated data
$name = mysqli_real_escape_string($con, $data->pagina);

// mysqli query to fetch all data from database
$query = "SELECT * from `content` where `name` = '$name'";

$result = mysqli_query($con, $query);
$arr = array();
if(mysqli_num_rows($result) != 0) {
	while($row = mysqli_fetch_assoc($result)) {
		$row['content'] = utf8_encode($row['content']);
		$arr[] = $row;
	}
}
// Return json array containing data from the databasecon
echo $json_info = json_encode($arr);
?>