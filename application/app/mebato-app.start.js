var mebato = angular.module('Mebato',['ui.bootstrap'])

mebato.controller('MainCTRL', function($scope,$modal,$http,$sce,$timeout){

	getPagina('index');


	//Functie die de boeken bibltioheek bijwerkt met een nieuw boek of een aangepaste boek.
	function getPagina(pagina){
		$http.post('application/db_files/get_content.php',{"pagina":pagina}).then(function(data){
			if (data.data != '') {
				setPagina(data.data);
			}
		});
	};

	function setPagina(data){
		$scope.pagina = data[0].name;
		$scope.content = $sce.trustAsHtml(data[0].content);
		$scope.header = $sce.trustAsHtml(data[0].header);

		if($scope.pagina == 'solutions'){

			var interval = setInterval(function(){
				if ($('[id*="item-last"]').length != 0) {
					clearInterval(interval);
					 //$(document).ready( function () {
			            var $grid = $( '.grid' ).masonry( {
			                itemSelector: '.grid-item',
			                columnWidth: '.grid-sizer',
			                gutter: 5
			            } );
			        //} );
				}
			},400);
			
			setTimeout(function(){
				clearInterval(interval);
			},5000);


			
		}
	};

	$scope.changePagina = function(pagina){
		console.log("test: " + pagina);
		getPagina(pagina);
	}

	//Functie om dingen te loggen op de console.
	$scope.log = function(parameter){
		console.log("Hello:" + JSON.stringify(parameter));
	};

//}]);
});