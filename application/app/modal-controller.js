mebato.controller('ModalInstanceCtrl',function($scope, $modalInstance, itemTeVerwijderen) {
	$scope.itemTeVerwijderen = itemTeVerwijderen;
	$scope.ok = function () {
		$modalInstance.close('Verwijderen');
	};
	$scope.cancel = function () {
		$modalInstance.dismiss('Annuleren');
	};
});