/*
	FILTERS
*/
// Filter om de taal te veranderen in een vlaggetje.
mebato.filter("getVlag", function(){
   return function(boekTaal, serieTaal){
   		if(boekTaal != '' && boekTaal != null){
   			var taal = boekTaal;
   		}else{
   			var taal = serieTaal;
   		}
   		switch(taal.toLowerCase()){
		   	case "duits":
		       	var output = "img/de.svg";
		        break;
		    case "engels":
		      	var  output = "img/gb.svg";
		        break;
		    case "nederlands":
		      	var  output =  "img/nl.svg";
		        break;
		    case "frans":
		       	var output = "img/fr.svg";
		        break;
		    case "gemixed":
		    	var output = "img/eu.svg";
		        break;
		    case "anders":
		    	var output = "img/eu.svg";
		        break;
		    default:
		     	var output = "img/nl.svg";
		        break;
		};

      return output; 
   }
});