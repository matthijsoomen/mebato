<?php session_start(); ?>
<!DOCTYPE html PUBLIC"-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Mebato b.v.</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Own CSS -->

        <link rel="stylesheet" href="public/scss/google-maps.scss"> 
        <link rel="stylesheet" href="public/css/style.css">
        <link rel="stylesheet" href="public/css/carousel.css">
        <link rel="stylesheet" href="public/css/bootstrap-extend.min.css" type="text/css">
        <script src="https://maps.google.com/maps/api/js?key=AIzaSyC4yeNR28SxOgqTQeYb_WiSKz5H2Wx8O9M" type="text/javascript"></script>

    </head>
    <body ng-controller="MainCTRL" ng-app="Mebato" class="homepage">

        <div ng-include src="'application/templates/header.php'" class="page-wrapper"></div>

        <div class="main-wrapper" id="main-wrapper">
            <a href="javascript:" class="btn btn-info hidden-md-up" id="toTop"><span class="glyphicon glyphicon-chevron-up"></span> Terug naar top</a>

            <section class="section-company pt-100 pb-50">
                <div class="container text-center">
                    <div ng-if="pagina == 'index'">
                        <span class="companyName">Mebato</span>
                        <br />
                        <span class="sloganName">Your Industrial Solution Partner</span>
                    </div>
                    <span ng-if="pagina != 'index'" class="heading" ng-bind-html="header"></span>

                </div>
            </section>

            <section class="section-content">
                <div class="content-wrapper p-50" ng-bind-html="content"></div>
            </section>

            <section ng-show="pagina == 'contact'" id="cd-google-map">
                <div class="content-wrapper-maps">
                    <div id="google-container"></div>
                    <div id="cd-zoom-in"></div>
                    <div id="cd-zoom-out"></div>
                </div>
            </section>
            
            <section ng-if="pagina == 'projects'">
                <div  id="myCarousel" ng-non-bindable class="carousel slide" data-ride="carousel" data-interval="5000">
                    <ol class="carousel-indicators" id="flat-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                        <li data-target="#myCarousel" data-slide-to="4"></li>
                        <li data-target="#myCarousel" data-slide-to="5"></li>
                        <li data-target="#myCarousel" data-slide-to="6"></li>
                        <li data-target="#myCarousel" data-slide-to="7"></li>
                        <li data-target="#myCarousel" data-slide-to="8"></li>
                        <li data-target="#myCarousel" data-slide-to="9"></li>
                        <li data-target="#myCarousel" data-slide-to="10"></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/4zx2oCU_lijl3z600/huge-industrial-factory-inside-robotic-machines-works_skldfcwc__F0000.png">
                            <div class="carousel-caption"> Klantspecifiek ontwerpen van een menglijn. Concept, basic en detail engineering hiervoor gedaan.
                            </div>
                        </div>
                        <div class="item">
                            <img src="http://aurak.ac.ae/en/wp-content/uploads/2016/05/Industrial-1-1.jpg">
                            <div class="carousel-caption"> Adviseren vervangen of optimaliseren van bedrijfskritische assets. Een transportband installatie analyseren op zijn kritieke aspecten en hoe deze te verbeteren.
                            </div>
                        </div>
                        <div class="item">
                            <img src="https://www.architectsjournal.co.uk/pictures/2000x2000fit/2/7/8/3047278_pg_14_151021_xxl_opera_3.jpg">
                            <div class="carousel-caption"> Ontwerpen van machine opstelling om te komen tot veilige opstelling en optimale logistiek van in- en uitvoerstromen. Installatie voor de zware industrie. </div>
                        </div>

                        <div class="item">
                            <img src="https://i.ytimg.com/vi/Bp4tGTNNi1I/maxresdefault.jpg">
                            <div class="carousel-caption"> Ontwerpen van een nieuwe speciaal machine op basis van verzamelde informatie en specificatie.
                            </div>
                        </div>

                        <div class="item">
                            <img src="http://www.elasticspace.com/wp-content/uploads/2014/05/InternetMachine10-web.jpg">
                            <div class="carousel-caption"> Haalbaarheidsonderzoek om proces installatie te optimaliseren, reduceren onderhouskosten en verbeteren veiligheid.
                            </div>
                        </div>

                        <div class="item">
                            <img src="http://mesc.solutions/upload/resyling.jpg">
                            <div class="carousel-caption"> Onze grondstof 'afval' verwerken tot een nuttig product, te denken aan alternatieve energie of nuttige halffabrikaten.
                            </div>
                        </div>

                        <div class="item">
                            <img src="https://www.bezner.com/wp-content/uploads/2017/07/Recycling-plant-for-industrial-and-demolition-waste.jpg">
                            <div class="carousel-caption"> Transportbanden in diverse uitvoeringen voor diverse applicaties ... wij willen u graag behulpzaam zijn bij de keuze, te denken aan; Vlakke banden | Staalkoordbanden | Boordbanden | PVC-PU banden enz.

                            </div>
                        </div>

                        <div class="item">
                            <img src="http://mesc.solutions/upload/resyling.jpg">
                            <div class="carousel-caption"> Ook bestaande logistieke processen verbeteren of veiliger te maken. Hiermee levensduur en flexibiliteit verhogen, denk hierbij ook aan het systematisch en modulair denken bij conceptueel ontwerp.
                            </div>
                        </div>


                        <div class="item">
                            <img src="http://www.motiontronic.co.za/wp-content/uploads/2017/05/Industrial-Automation-South-Africa.jpg">
                            <div class="carousel-caption"> Rollenbanen in alle denkbare varianten. Aangedreven, zwaartekracht gedreven, draaiplateua's enz enz. Om voor u te dimensioneren maar ook prijs gunstig leveren behoort tot de mogelijkheden.
                            </div>
                        </div>

                        <div class="item">
                            <img src="http://mesc.solutions/upload/resyling.jpg">
                            <div class="carousel-caption"> Advies, ontwerp, realisatie en leveren van industriele drogers van stortgoederen. Van laboratorium tot realisatie. Ook optimalisatie van droogprocessen om tot energie reductie te komen. Kortom voor al uw droog vragen kunnen wij u helpen.
                            </div>
                        </div>

                    </div><a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a><a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>
                </div>

            </div>
        </section>


        <div ng-include src="'application/templates/footer.php'"></div>



        <!-- Angular Material requires Angular.js Libraries -->
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.1/angular.min.js" ></script>
        <script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>
            
        <!-- Bootstrap Material Library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <!-- Angular App JS -->
        <script src="application/app/mebato-app.start.js"></script>
        <script src="application/app/mebato-filters.js"></script>
        <script src="application/app/modal-controller.js"></script>
        <script src="public/js/scroll-up.js"></script>

        <script src="public/js/google-maps.js"></script>
        <script src="public/js/header.js"></script>

    <script src="https://masonry.desandro.com/masonry.pkgd.js"></script>
       

<style>
    * {
            box-sizing: border-box;
        }
        
        .grid {
            max-width: 1115px;
            margin: 0 auto;
        }
        
        .grid-sizer,
        .grid-item {
            width: 275px;
        }
        
        .grid-item img {
            max-width: 100%;
            border-radius: 5px;
            padding-top: 5px;
            padding-left: 0;
        }
        
        .gallery-divider {
            border: 1px solid;
        }
        
        #img-header {
            height: 10%;
            width: 20%;
            display: block;
            margin-left: auto;
            margin-right: auto;
        }
    </style>


    </body>
</html>